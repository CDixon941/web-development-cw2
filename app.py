

# TO RUN THE APP, DO python app.py


# flask dependencies
from flask import Flask, render_template, request, redirect, url_for, flash, session, jsonify
from flask_login import LoginManager, login_user, login_required, logout_user
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Api
from flask_socketio import SocketIO, send, join_room, leave_room, emit

import os
from werkzeug.security import generate_password_hash, check_password_hash
# python dependencies
import json
from datetime import datetime

from logging.config import dictConfig

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

# initialise the app and the database
app = Flask(__name__)
api = Api(app)
app.config.from_object('config')
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
db = SQLAlchemy(app)
db.create_all()
migrate = Migrate(app, db)

socketio = SocketIO(app, manage_session=False)

from models import *
from form import *
from functions import *

LoginManager = LoginManager()
LoginManager.init_app(app)


# set up routes for socket io

@socketio.on('message')
def handleMessage(msg, sender):
    print("message sent")
    socketio.emit('message', (msg, sender), room=session['current_chat'])


@socketio.on('join')
@login_required
def joinroom():
    join_room(session['current_chat'])
    print("joined")


@socketio.on('leave')
@login_required
def leaveroom():
    if session.get('current_chat'):
        leave_room(session['current_chat'])
        print("left")


@LoginManager.user_loader
def load_user(username):
    return accounts.query.filter_by(username=username).first()


@app.route('/', methods=['GET', 'POST'])
def home():
    login = user()
    if login.validate_on_submit():

        # if the username exists
        data = accounts.query.filter_by(username=str(login.username.data)).first()
        if data:

            # if the password matches the one in the database
            if check_password_hash(data.password, login.password.data):
                username = login.username.data
                login_user(data)

                app.logger.info('%s logged into their account successfully', username)

                return redirect(url_for('home_page', username=username))

            else:
                app.logger.info('%s failed to login to their account', login.username.data)
                flash('Incorrect Password!')
        else:
            app.logger.info('an attepmt was made by %s to loginto their account. It does not exist',
                            login.username.data)
            flash('There is no account associated with that username. Please create one by pressing the link.')
    return render_template("login.html", title="Login", main_header="Login Page", user_login=login)


@app.route('/createuser/', methods=['GET', 'POST'])
def new_user():
    signup = user()
    if signup.validate_on_submit():

        username = str(signup.username.data)
        password = generate_password_hash(str(signup.password.data))
        email_address = str(signup.email_address.data)

        # server side validation as these fields cannot be blank
        if username and password and email_address:

            error = password_check(password)
            print(error)

            if error:
                flash(error)

            # add the new user to the database and redirect them to their new home page
            else:
                new = accounts(username=username, password=password, email_address=email_address)
                db.session.add(new)
                db.session.commit()
                login_user(new)
                app.logger.info('%s created a new account', username)
                return redirect(url_for('home_page', username=username))
        else:
            flash("Please fill out the full form!")
    return render_template("login.html", title="Create Account", main_header="Create Account", user_login=signup)


@app.route('/homepage/', methods=['GET', 'POST'])
@login_required
def home_page():
    username = request.args.get('username')
    if username is None:
        username = session['usr']
    print(username)
    # set a session variable to be the username
    session['usr'] = username
    if request.method == 'POST':

        if request.is_json:
            data = json.loads(request.data)
            password = data.get('data')
            print(password)
            password = generate_password_hash(str(password))

            query = accounts.query.filter_by(username=session['usr']).first()
            query.password = password
            db.session.commit()

            return "changed password"
        else:
            if request.form['nav_button'] == 'Logout':

                # log the user out using flask logout
                return redirect(url_for('logout'))
            else:
                # go to the main messaging page
                return redirect(url_for('friends', username=username))

    return render_template("homepage.html", username=username)


@app.route('/respond', methods=['POST'])
@login_required
def respond():
    data = json.loads(request.data)
    response = data.get('search')
    current_user = data.get('username')
    search = accounts.query.filter_by(username=str(response)).first()

    found = 0

    # add the person exists in the database
    if search:
        friend = search.username

        # if they searched them selves
        if friend == current_user:
            response = "You have searched for your self!"

        else:
            # if they added a valid person
            check_friend = friendships.query.filter_by(username=current_user, friend=friend).first()
            check_back = friendships.query.filter_by(username=friend, friend=current_user).first()

            if check_friend or check_back:
                response = "You are already friends!"
            else:
                response = friend
                found = 1
                # send the response back to ajax so that the button to add friend becomes visible

    else:
        response = "User does not exist!"

    return json.dumps({'status': 'OK', 'friend': response, 'found': found})


@app.route('/addfriend', methods=['POST'])
@login_required
def addfriend():
    data = json.loads(request.data)
    username = data.get('user')
    friend = data.get('friend')

    friendship_id = username + friend

    # add the new friendship to the database
    data = friendships(username=username, friend=friend, friendship_id=friendship_id)
    db.session.add(data)
    db.session.commit()
    app.logger.info(username + " added " + friend + " to their friends list")

    return json.dumps({'status': 'OK', 'friend': friend})


@app.route('/friends', methods=['GET', 'POST'])
@login_required
def friends():
    username = request.args.get('username', None)
    message_form = sendmessage()
    friends = friendships.query.filter_by(username=username).all()
    other_friends = friendships.query.filter_by(friend=username).all()

    if request.method == 'POST':
        username = session['usr']

        # if it is a ajax data post
        if request.is_json:
            data = json.loads(request.data)

            # if the data sent was selecting a friend to message
            if data.get('friend'):

                response = data.get('friend')
                session['tomsg'] = response
                friend1 = access_list.query.filter_by(user_name=username).all()
                friend2 = access_list.query.filter_by(user_name=response).all()

                access = 0
                chat_id = ""

                # if a chat already exists then query the messages and send it to ajax
                for i in friend1:
                    for j in friend2:
                        if i.chat_name == j.chat_name:
                            chat_id = i.chat_name
                            access = 1

                if access == 1:
                    session['current_chat'] = chat_id
                    msgs = message.query.filter_by(chat_id=chat_id).all()
                    message_data = []

                    for message_id in msgs:
                        message_data.append(message_id.username_id)
                        message_data.append(message_id.message_text)

                    message_data = jsonify(message_data)
                    app.logger.info(username + " selected" + response + " to message")
                    return message_data

            # if the request is a message (text)
            if data.get('text'):
                text = data.get('text')

                # if a recipient is selected then query if a chat exists
                if session.get('tomsg'):
                    response = session['tomsg']
                    friend1 = access_list.query.filter_by(user_name=username).all()
                    friend2 = access_list.query.filter_by(user_name=response).all()
                    access = 0
                    chat_id = ""

                    for i in friend1:
                        for j in friend2:
                            if i.chat_name == j.chat_name:
                                chat_id = i.chat_name
                                access = 1

                    # if a chat exists then add it to the database and send the message to the socket io handler
                    if access == 1:
                        response = session['tomsg']
                        msg_id = str(datetime.now()) + username + "msg" + response
                        new_message = message(message_id=msg_id, username_id=username, chat_id=chat_id,
                                              message_text=text)
                        db.session.add(new_message)
                        db.session.commit()
                        handleMessage(text, username)
                        app.logger.info(username + " messaged " + response + " : " + text)
                        flash("Message successfuly sent to: " + response)

                    # if it does not exist then create a new chat, users access and then add the message and send it
                    # to the socket io handler
                    else:
                        response = session['tomsg']
                        chat_id = username + "msg" + response
                        new_chat = chat(chat_id=chat_id)
                        msg_id = str(datetime.now()) + username + "msg" + response
                        new_message = message(message_id=msg_id, username_id=username, chat_id=chat_id,
                                              message_text=text)

                        new_access = access_list(chat_name=chat_id, user_name=username,
                                                 access_id=chat_id + username)
                        other_access = access_list(chat_name=chat_id, user_name=response,
                                                   access_id=chat_id + response)

                        db.session.add(new_chat)
                        db.session.add(new_message)
                        db.session.add(new_access)
                        db.session.add(other_access)
                        db.session.commit()
                        handleMessage(text, username)
                        app.logger.info(username + " messaged " + response + " : " + text)
                        flash("Message successfuly sent to: " + response)

                else:
                    flash("You need to select someone to message!")

        else:

            if request.form['button'] == "Home":
                return redirect(url_for('home_page', username=username))

    return render_template("friends.html", username=username, friends=friends, other_friends=other_friends,
                           messageme="Select someone to message!", messageform=message_form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))


if __name__ == '__main__':
    socketio.run(app, debug=True)
