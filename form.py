from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField
from wtforms.validators import DataRequired


class user(FlaskForm):
    email_address = StringField('email_address')
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])

class sendmessage(FlaskForm):
    text = StringField('messagethis',validators=[DataRequired()])