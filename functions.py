import re


def password_check(password):
    error = ""

    if len(password) < 8:
        error = error + " Password must be longer than 7 characters."

    if re.search(r"[A-Z]", password) is None:
        error = error + " Password must contain at least 1 uppercase letter."

    if re.search(r"[a-z]", password) is None:
        error = error + " Password must contain at least 1 lowercase letter."

    if re.search(r"\d", password) is None:
        error = error + " Password must contain at least 1 number."

    return error
