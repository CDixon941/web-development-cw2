from app import db
from flask_login import UserMixin

access = db.Table('access',
                  db.Column('chat_name', db.String(500), db.ForeignKey('chat.chat_id'), primary_key=True),
                  db.Column('user_name', db.String(500), db.ForeignKey('accounts.username'), primary_key=True)

                  )


class access_list(db.Model):
    chat_name = db.Column(db.String(500))
    user_name = db.Column(db.String(500))
    access_id = db.Column(db.String(500), primary_key=True)


class accounts(db.Model, UserMixin):
    username = db.Column(db.String(500), primary_key=True)
    email_address = db.Column(db.String(500))
    password = db.Column(db.String(500))
    message_id = db.relationship('message', backref='accounts', lazy='dynamic')

    def is_active(self):
        return True

    def get_id(self):
        return self.username

    def is_authenticated(self):
        return True


class chat(db.Model):
    chat_id = db.Column(db.String, primary_key=True)
    message_id = db.relationship('message', backref='chat', lazy='dynamic')
    access = db.relationship('accounts', secondary=access, lazy='subquery', backref=db.backref('chats', lazy=True))


class friendships(db.Model):
    username = db.Column(db.String(500))
    friend = db.Column(db.String(500))
    friendship_id = db.Column(db.String(500), primary_key=True)


class message(db.Model):
    message_id = db.Column(db.String(500), primary_key=True)
    username_id = db.Column(db.String(500), db.ForeignKey('accounts.username'))
    chat_id = db.Column(db.String(500), db.ForeignKey('chat.chat_id'))
    message_text = db.Column(db.String(500))
