var current;

$(document).ready(function(){
   $("#change_password_form").submit(function () {
       var data = $("#change_password_input").serializeArray().map(function(v) {return v.value;} );
       data  = data.toString();
        console.log(data);
        alert("Password Changed!");

       $.ajax({
          url:'/homepage/',
          type: 'POST',
          data: JSON.stringify({data:data}),
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           success: function () {
           }
       });
   });
});



$(document).ready(function(){

    $("#search_form").submit(function (e) {
        // get what name was entered in the add friend bar
        var data = $("#search_text").serializeArray().map(function(v) {return v.value;} );
        data  = data.toString();

        // get the current user
        var current_user = localStorage.getItem("user");

        $.ajax({
            url: '/respond',
            type: 'POST',
            data: JSON.stringify({ search: data ,username: current_user}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response){
                if(response.found===1){

                    // if found, store the selected person and display an add button
                    localStorage.setItem("potential_friend",response.friend);
                    $("#friend_result").text("Click 'Add Friend' if you would like to add  '" + response.friend + "'  to your friends list.");
                    $("#add_friend").css('visibility','visible');
                }
                else{
                    $("#friend_result").text(response.friend);
                }
            }
        });
    });
});

$(document).ready(function(){

    // when the add friend button is clicked, send a response to flask and inform the user of the success.
    $("#add_friend").on("click", function () {
        $.ajax({
             url: '/addfriend',
            type: 'POST',
            data: JSON.stringify({ user:localStorage.getItem("user"),friend:localStorage.getItem("potential_friend")}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",

            success: function (response) {
                 $("#friend_result").text(response.friend + "  has been added to your friends list!");
                 $("#add_friend").css('visibility','hidden');
            }
        });
    });
});

$(document).ready(function () {
   $(".message_friend").on("click",function () {
       size=0;
        var select_friend = $(this).attr("id");
        $("#submitmessage").text("Send message to "+select_friend.toString());

        $("#fromthem").empty();
        $("#fromyou").empty();

        // connect to socket io

        var socket = io.connect("http://127.0.0.1:5000/");
        //var socket = io.connect("https://morning-beyond-01456.herokuapp.com/");

        socket.on('connect', function(){
            console.log("connected");
        });

        // if a different user was selected, leave the chat room and join a new one

        if (current !=select_friend){
                    socket.emit('leave')
                }

        socket.emit('join');

        // get the new message from message and ping it to the sender and receiver
        $("#messageform").on("submit",function () {
        socket.on("message",function (msg,sender) {

                    console.log("sent message");

                    if(localStorage.getItem("user")==sender){
                    var node = document.createElement("LI");
                    var textnode = document.createTextNode(msg);
                    node.appendChild(textnode);
                    document.getElementById("fromyou").appendChild(node);

                    var node = document.createElement("LI");
                    var textnode = document.createTextNode("---");
                    node.appendChild(textnode);
                    document.getElementById("fromthem").appendChild(node);
                    }
                    else{
                        var node = document.createElement("LI");
                    var textnode = document.createTextNode("---");
                    node.appendChild(textnode);
                    document.getElementById("fromyou").appendChild(node);

                    var node = document.createElement("LI");
                    var textnode = document.createTextNode(msg);
                    node.appendChild(textnode);
                    document.getElementById("fromthem").appendChild(node);
                    }
                });
            });

        $.ajax({
            url: '/friends',
            type: 'POST',
            data: JSON.stringify({ user:localStorage.getItem("user"),friend:select_friend}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                 current = select_friend;
                 var node = document.createElement("LI");
                    var textnode = document.createTextNode(localStorage.getItem("user"));
                    node.appendChild(textnode);
                    document.getElementById("fromyou").appendChild(node);

                 var node = document.createElement("LI");
                    var textnode = document.createTextNode(select_friend);
                    node.appendChild(textnode);
                    document.getElementById("fromthem").appendChild(node);

                // an array of strings
                for (let i in response){
                    // if the message was sent by the current user then put in their column, else put it in other one
                    if(response[i]==localStorage.getItem("user")){

                        // i is increased so that the name of the sender is not displayed all the time
                        i++;
                    var node = document.createElement("LI");
                    var textnode = document.createTextNode(response[i]);
                    node.appendChild(textnode);
                    document.getElementById("fromyou").appendChild(node);

                    var node = document.createElement("LI");
                    var textnode = document.createTextNode("---");
                    node.appendChild(textnode);
                    document.getElementById("fromthem").appendChild(node);
                    }



                    if(response[i]==select_friend){
                        i++;
                    var node = document.createElement("LI");
                    var textnode = document.createTextNode(response[i]);
                    node.appendChild(textnode);
                    document.getElementById("fromthem").appendChild(node);

                    var node = document.createElement("LI");
                    var textnode = document.createTextNode("---");
                    node.appendChild(textnode);
                    document.getElementById("fromyou").appendChild(node);
                    }
                }

            }
        });
   }) ;
});

$(document).ready(function () {
   $("#messageform").on("submit",function () {

        var data = $("#msg_text").serializeArray().map(function(v) {return v.value;} );
        data = data.toString();
        // send the text of the message to flask
        $.ajax({
            url: '/friends',
            type: 'POST',
            data: JSON.stringify({ user:localStorage.getItem("user"),text:data}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
     });
   });
});