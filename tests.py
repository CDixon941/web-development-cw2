# flask dependencies
import unittest
from datetime import datetime

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Api

import os

# initialise the app and the database

app = Flask(__name__)
api = Api(app)
app.config.from_object('config')
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
db = SQLAlchemy(app)

from models import *


def addUser(email_address, username, password):
    new = accounts(email_address=email_address, username=username, password=password)
    return new


def addChat():
    new = accounts(email_address="a", username="b", password="c")
    new2 = accounts(email_address="x", username="y", password="z")
    chat_id = new.username + "msg" + new2.username

    return new, new2, chat_id


def addAccess():
    new = accounts(email_address="a", username="b", password="c")
    new2 = accounts(email_address="x", username="y", password="z")
    chat_id = new.username + "msg" + new2.username
    newaccess = access_list(chat_name=chat_id, user_name=new.username, access_id=chat_id + new.username)
    new2access = access_list(chat_name=chat_id, user_name=new2.username, access_id=chat_id + new2.username)

    return newaccess, new2access


def addMessage():
    new = accounts(email_address="a", username="b", password="c")
    new2 = accounts(email_address="x", username="y", password="z")
    chat_id = new.username + "msg" + new2.username

    msg_id = str(datetime.now()) + new.username + "msg" + new2.username

    newmsg = message(message_id=msg_id, username_id=new.username, chat_id=chat_id, message_text="text")

    return newmsg


class TestCase(unittest.TestCase):
    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()

    def test_add_user(self):
        db.session.add(addUser("x", "y", "z"))
        db.session.commit()
        query = accounts.query.filter_by(username="y").all()
        x = accounts.query.filter_by(username="y").first()
        db.session.delete(x)
        db.session.commit()
        for i in query:
            assert i.email_address == "x"
            assert i.username == "y"
            assert i.password == "z"

    def test_add_chat(self):
        new, new2, chat_id = addChat()
        db.session.add(chat(chat_id=chat_id))
        db.session.commit()

        query = chat.query.filter_by(chat_id=chat_id).all()
        delete = chat.query.filter_by(chat_id=chat_id).first()

        for i in query:
            assert i.chat_id == new.username + "msg" + new2.username
        db.session.delete(delete)
        db.session.commit()

    def test_add_acceess(self):
        new1, new2 = addAccess()
        db.session.add(new1)
        db.session.add(new2)
        db.session.commit()

        query = access_list.query.filter_by(user_name=new1.user_name).all()
        query2 = access_list.query.filter_by(user_name=new2.user_name).all()

        i = 0

        for i in query:
            for j in query2:
                if i.chat_name == j.chat_name:
                    i = 1
        query = access_list.query.filter_by(user_name=new1.user_name).first()
        query2 = access_list.query.filter_by(user_name=new2.user_name).first()
        db.session.delete(query)
        db.session.delete(query2)
        db.session.commit()

        assert i == 1

    def test_add_msg(self):
        new1 = addMessage()
        db.session.add(new1)
        db.session.commit()

        query = message.query.filter_by(message_id=new1.message_id).all()

        i = 0
        for i in query:
            if i.message_text == "text":
                i = 1

        query = message.query.filter_by(message_id=new1.message_id).first()
        db.session.delete(query)
        db.session.commit()

        assert i == 1


if __name__ == '__main__':
    unittest.main()
